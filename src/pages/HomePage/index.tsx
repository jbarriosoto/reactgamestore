

import '../../modules/common/styles/App.css'
import '../../modules/common/styles/Card.css'
import Card from "../../modules/common/components/cards/Card"
import { useFetchGames } from '../../modules/games/hooks/useFetchGames'

const Store = () => {
    const data = useFetchGames();

  return (

    <div>
       <Card {...data} />
    </div>
  );
}

export default Store;

