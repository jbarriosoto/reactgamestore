import { Route, Switch } from "react-router-dom";
import DetailGame from '../pages/DetailGamePage/DetailGame'
import Store from '../pages/HomePage/index'

function Index() {

    return (
      <Switch>
        <Route exact path="/">
          <Store />
        </Route>
        <Route path="/detail" children={<DetailGame />} />
      </Switch>
    );
}

export default Index;