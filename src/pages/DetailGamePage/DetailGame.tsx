import {  useLocation } from "react-router-dom";
import '../../modules/common/styles/DetailGame.css'
import { useFetchGames } from "../../modules/games/hooks/useFetchGames";
import iconNew from '../../modules/common/images/icons/new.png'
import { Game } from "../../modules/games/types/Game";


const DetailGame = () => {
    
    //Extraemos los datos de los juegos a partir de un hook personalizado
    const data = useFetchGames();

    //Usamos el hook useLocation para conocer cuando cambia la URL
    const location = useLocation();
    
    return ( 
        <div className="containerDetail">
             {data.filter((product:Game) => product.id === location.state['detail'] ).map((filteredProduct:Game) => (
                <div className="mainSection" key={ filteredProduct.id }>  
                        <img src={ filteredProduct.banner_detail_game } className="imageBanner" alt="banner-game"/> 
                        <div>
                            <img src={ iconNew } className="iconDetail" alt="icon-new-game"/>
                            <span className="new-release-tag"> NEW RELEASE </span>
                        </div>
                        <span className="titleGame">{ filteredProduct.title }</span>
                    <div className="info-price-install"> { filteredProduct.price} {filteredProduct.currency }
                        <button className="rectangle-install"> INSTALL GAME </button> 
                    </div>
                    <div className="infoGame">
                            <img src={ filteredProduct.description_image_detail_game } className="imageDetail" alt="banner-game"/>
                        <div className="text">
                            <p className="descriptionGame"> { filteredProduct.description } </p>
                        </div>
                    </div>
                </div>
                ))}
        </div>
    );
}

export default DetailGame;

