
//Interfaz creada para posteriormente hacer match de atributos con sus respectivos tipos
export interface Game {
    front_cover_image: string,
    banner_detail_game: string,
    description_image_detail_game: string,
    title: string,
    id: string,
    price: number,
    currency: string,
    description: string,
    short_description: string
}

