import { useState } from 'react';

export function useFetchGames(){

    /*
    * Extraemos los datos de los juegos de un json
    * y creamos la variable de estado donde guardaremos los datos extraidos de json
    */
     
    const [data] = useState(require('../../../modules/common/datasource/products.json'));

    return data;

}