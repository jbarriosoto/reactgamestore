import { render, screen } from '@testing-library/react';
import { Card } from '../../../../common/components/cards/Card';

test('can render Card', () => {
    render(<Card  />); 
  });

test('can render elements in a card', () => {
  render(<Card />);
    const linkElement = screen.getByText(/BESTSELLERS/i); 
    expect(linkElement).toBeInTheDocument();
});

test('can render list in a card', () => {
    render(<Card />);
    expect(screen.getAllByRole('list'));

   // expect(screen.getAllByRole(/button/)).toHaveClass('rectangle-install-games');
  });


