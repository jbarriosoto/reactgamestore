import { CCarousel, CCarouselItem  } from '@coreui/react';
import '../../styles/Banner.css'
import primeraImagenBanner from '../../images/banner_homepage/horizon-zero-dawn-banner-homepage.jpg';
import terceraImagenBanner from '../../images/banner_homepage/league-of-legends-background.jpg';
import segundaImagenBanner from '../../images/banner_homepage/god-of-war-banner-homepage.jpg';
import { Game } from '../../../games/types/Game';

export const Banner = ({data}) => {

    // Filtramos los datos pasados por props y  seleccionamos aquel que coincide con el id del juego correspondiente
    const slide1:Game = data.filter((game: Game) => game.id === '1');
    const slide2:Game = data.filter((game: Game) => game.id === '2');
    const slide3:Game = data.filter((game: Game) => game.id === '3');


    return (
        <CCarousel indicators>
            <CCarouselItem>
              <img className="d-block w-100" src={primeraImagenBanner} alt="slide 1 banner principal" />
              <div>
                <span className="title-banner"> { slide1[0].title } </span>
                <p className="description-banner"> { slide1[0].short_description } </p>
              </div>
              <button className="rectangle-install-games"> INSTALL GAME </button> 
              <button className="rectangle-favorite-game"> ADD TO FAVORITES</button> 
            </CCarouselItem>
            <CCarouselItem>
              <img className="d-block w-100" src={segundaImagenBanner} alt="slide 2 banner principal" />
              <div>
                <span className="title-banner"> { slide2[0].title } </span>
                <p className="description-banner"> { slide2[0].short_description } </p>
              </div>
              <button className="rectangle-install-games"> INSTALL GAME </button> 
              <button className="rectangle-favorite-game"> ADD TO FAVORITES</button> 
            </CCarouselItem>
            <CCarouselItem>
              <img className="d-block w-100" src={terceraImagenBanner} alt="slide 2 banner principal"  />
              <div>
                <span className="title-banner"> { slide3[0].title } </span>
                <p className="description-banner"> { slide3[0].short_description } </p>
              </div>
              <button className="rectangle-install-games"> INSTALL GAME </button> 
              <button className="rectangle-favorite-game"> ADD TO FAVORITES</button> 
            </CCarouselItem>
        </CCarousel>
    );
} 