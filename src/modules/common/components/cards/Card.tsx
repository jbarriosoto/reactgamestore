
import '../../styles/Card.css'
import { useHistory } from "react-router-dom";
import { useFetchGames } from '../../../games/hooks/useFetchGames';
import {Banner} from '../banners/Banner';
import { Game } from '../../../games/types/Game';

export const Card = () => {
    
    //Extraemos los datos de los juegos a partir de un hook personalizado
    const data = useFetchGames();

    //Usamos el hook useLocation para conocer cuando cambia la URL
    let history = useHistory();

    //Obtenemos el parametro del id a partir de la URL mediante el hook history creado anteriormente.
    function getParamId(value:String){
            history.push({
                pathname: '/detail',
                search: `productId=${value}`,
                state: { detail: value }
            });
    }
  
    return (
        <div className="App-header">                      
            <Banner data={data} />
                <div className="bestsellers">
                    <p className="bestsellers"> BESTSELLERS </p>
                </div>
                {data.map((product:Game) => {
                    return (
                        <div key={ product.id } className="column">
                            <div aria-label="card" key={ product.id } className="card">
                                <div className="imageGame"> 
                                    <img src={ product.front_cover_image } className="imageGame" alt="banner-game"/>
                                </div>
                                <div className="container">
                                    <span className="card-title">{ product.title }</span>
                                    <span className="card-price"> { product.price } { product.currency }</span>
                                    <button value={product.id} className="rectangle-buy" onClick={e => getParamId(e.currentTarget.value)}> BUY NOW </button> 
                                </div>
                            </div>
                        </div>
                    );
                })}
        </div>   
    );
}

export default Card;