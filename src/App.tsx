
import './modules/common/styles/App.css'
import Index from "./pages/index"
import { BrowserRouter } from "react-router-dom";

export default function App() {

  return (
      <BrowserRouter>
            <Index />
      </BrowserRouter>

  );
}

